<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 13.03.14
 * Time: 6:55
 */

namespace Tasks\TaskAdapter;
use Tasks\AbstractTask;
use Tasks\ItemTask;

interface ITaskAdapter {

    /**
     * @param AbstractTask $task
     * @param $queue
     * @param $runTime
     * @internal param $periodTime
     * @return boolean
     */
    public function addTask(AbstractTask $task, $queue, $runTime = null);

    /**
     * @param AbstractTask $task
     * @return boolean
     */
    public function hasTask(AbstractTask $task);

    /**
     * @param AbstractTask $task
     * @return boolean
     */
    public function removeTask(AbstractTask $task);

    /**
     * @param AbstractTask $task
     * @param $nextTime
     * @return boolean
     */
    public function touchTask(AbstractTask $task, $nextTime);

    /**
     * @param $consumerName
     * @param $limit
     * @return array
     */
    public function getTasksForProcess($consumerName, $limit);
}