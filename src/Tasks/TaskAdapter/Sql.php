<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 16.03.14
 * Time: 1:00
 */

namespace Tasks\TaskAdapter;
use Tasks\AbstractTask;

class Sql implements ITaskAdapter{

    /**
     * @var \Zend_Db_Adapter_Abstract
     */
    protected $db;

    /**
     * @var string
     */
    protected $table;

    protected $dateFormat = 'Y-m-d H:i:s';

    public function __construct(\Zend_Db_Adapter_Abstract $db, $table)
    {
        $this->db = $db;
        $this->table = $table;
    }

    /**
     * Add task
     * @param AbstractTask $task
     * @param $queue
     * @param $runTime
     * @internal param $periodTime
     * @return boolean
     */
    public function addTask(AbstractTask $task, $queue, $runTime = null)
    {
        if(!$runTime) {
            $runTime = time();
        }

        $result = $this->db->insert($this->table, array(
            'id' => $task->getId(),
            'task' => get_class($task),
            'params' => json_encode($task->getParams()),
            'date' => date($this->dateFormat, $runTime),
            'queue' => $queue
        ));
        return (boolean) $result;
    }

    /**
     * Checks for the existence of a task
     * @param AbstractTask $task
     * @return bool
     */
    public function hasTask(AbstractTask $task)
    {
        if($this->db->fetchOne("SELECT * FROM {$this->table} WHERE id = ?", array($task->getId()))) {
            return true;
        }
        return false;
    }

    /**
     * Delete task
     * @param AbstractTask $task
     * @return boolean
     */
    public function removeTask(AbstractTask $task)
    {
        $result = $this->db->delete($this->table, array(
            'id=?' => $task->getId()
        ));
        return (boolean) $result;
    }

    /**
     * Extends life time
     * @param AbstractTask $task
     * @param $nextTime
     * @return boolean
     */
    public function touchTask(AbstractTask $task, $nextTime)
    {
        $result = $this->db->update($this->table, array(
            'date' => date($this->dateFormat, $nextTime),
            'consumer' => null
        ), array(
            'id=?' => $task->getId()
        ));
        return (boolean) $result;
    }

    /**
     * @param $consumerName
     * @param $limit
     * @return array
     */
    public function getTasksForProcess($consumerName, $limit)
    {
        if(!$result = $this->fetchTasks($consumerName, $limit)) {

            //Make tasks for complete
            $this->db->update(
                $this->table,
                array('consumer' => $consumerName),
                array('date <= ?' => date($this->dateFormat), 'consumer IS NULL')
            );

            return $this->fetchTasks($consumerName, $limit);
        }

        return $result;
    }

    /**
     * @param string $consumerName
     * @param int $limit
     * @return array
     */
    protected function fetchTasks($consumerName, $limit)
    {
        $result = $this->db->fetchAll(
            "SELECT * FROM {$this->table} WHERE consumer = ? ORDER BY date LIMIT ?",
            array($consumerName, $limit)
        );

        if($result) {
            $tasks = array();
            foreach($result as $item) {
                $task = $item;
                $task['params'] = json_decode($task['params']);
                $task['date'] = strtotime($task['date']);

                $tasks[] = $task;
            }
            return $tasks;
        }

        return [];
    }
} 