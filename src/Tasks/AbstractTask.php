<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 13.03.14
 * Time: 6:58
 */

namespace Tasks;


abstract class AbstractTask {

    protected $params = array();

    abstract public function getId();

    /**
     * @return null|int
     */
    public function getPeriod()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    abstract public function run(\Zend_Log $log);
}