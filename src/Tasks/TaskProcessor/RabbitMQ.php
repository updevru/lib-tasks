<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 16.03.14
 * Time: 11:04
 */

namespace Tasks\TaskProcessor;
use Tasks\AbstractTask;
use Tasks\ProcessState;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Exception;

class RabbitMQ implements ITaskProcessor {

    /**
     * @var \Zend_Log
     */
    protected $log;

    /**
     * @var array
     */
    protected $options = array(
        'host' => 'localhost',
        'port' => 5672,
        'user' => 'guest',
        'password' => 'guest',
        'vhost' => '/',
        'messages_waiting' => 1800,
        'insist' => false,
        'login_method' => 'AMQPLAIN',
        'login_response' => null,
        'locale' => 'en_US',
        'connection_timeout' => 3.0,
        'read_write_timeout' => 3.0,
        'context' => null,
        'keepalive' => false,
        'heartbeat' => 0
    );

    /**
     * @var AMQPConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;
    protected $declareQueue = array();

    /**
     * @var ProcessState
     */
    protected $processState;

    public function __construct(\Zend_Log $log, ProcessState $processState, array $options)
    {
        $this->log = $log;
        $this->processState = $processState;
        $this->options = array_merge($this->options, $options);
    }

    public function __destruct()
    {
        if($this->channel) {
            $this->channel->close();
        }

        if($this->connection) {
            $this->connection->close();
        }
    }

    /**
     * @param $queue
     * @return AMQPChannel
     */
    protected function getChannel($queue)
    {
        if(!$this->channel) {
            $this->connection = new AMQPStreamConnection(
                $this->options['host'],
                $this->options['port'],
                $this->options['user'],
                $this->options['password'],
                $this->options['vhost'],
                $this->options['insist'],
                $this->options['login_method'],
                $this->options['login_response'],
                $this->options['locale'],
                $this->options['connection_timeout'],
                $this->options['read_write_timeout'],
                $this->options['context'],
                $this->options['keepalive'],
                $this->options['heartbeat']
            );
            $this->channel = $this->connection->channel();
            $this->log->log("RabbitMQ connect {$this->options['host']}:{$this->options['port']}{$this->options['vhost']}", \Zend_Log::INFO, array('queue' => $queue));
        }

        if(!in_array($queue, $this->declareQueue)) {
            $this->channel->queue_declare($queue, false, true, false, false);
            $this->declareQueue[] = $queue;
            $this->log->log("RabbitMQ queue declare '{$queue}'", \Zend_Log::INFO, array('queue' => $queue));
        }

        return $this->channel;
    }

    /**
     * @param AbstractTask $task
     * @param $queue
     * @return boolean
     */
    public function doTask(AbstractTask $task, $queue)
    {
        $data = array(
            'task' => get_class($task),
            'params' => $task->getParams()
        );

        $msg = new AMQPMessage(json_encode($data), array('delivery_mode' => 2));
        $this->getChannel($queue)->basic_publish($msg, '', $queue);

        $this->log->log("Push task '{$task->getId()}'", \Zend_Log::INFO, array('queue' => $queue));
    }

    /**
     * @param $queue
     */
    public function work($queue)
    {
        $log = $this->log;
        $log->setEventItem('queue', $queue);
        $log->log("Start RabbitMQ processor", \Zend_Log::INFO);

        $callback = function(AMQPMessage $msg) use ($queue, $log) {
            $data = json_decode($msg->body, true);
            $class = new \ReflectionClass($data['task']);
            /** @var AbstractTask $task */
            $task  = $class->newInstanceArgs($data['params']);

            $log->log("Start process {$task->getId()}", \Zend_Log::INFO);
            try {
                $task->run($log);
            } catch (\Exception $e) {
                $msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
                $log->log("Exiting process with error {$data['task']} ({$task->getId()})", \Zend_Log::DEBUG);
                throw $e;
            }
            $log->log("Finish process {$data['task']} ({$task->getId()})", \Zend_Log::DEBUG);

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        };

        $channel = $this->getChannel($queue);
        $channel->basic_consume($queue, '', false, false, false, false, $callback);

        while(count($channel->callbacks)) {
            if(!$this->processState->isWork()) {
                $log->log("Break process", \Zend_Log::DEBUG);
                break;
            }
            $log->log("Start wait messages", \Zend_Log::INFO);
            try {
                $channel->wait(null, false, $this->options['messages_waiting']);
            } catch (Exception\AMQPTimeoutException $e) {
                $log->log("Stop wait", \Zend_Log::INFO);
                break;
            }
        }
    }

} 