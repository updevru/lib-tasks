<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 13.03.14
 * Time: 7:26
 */

namespace Tasks\TaskProcessor;
use Tasks\AbstractTask;

interface ITaskProcessor {

    /**
     * @param AbstractTask $task
     * @param $queue
     * @return boolean
     */
    public function doTask(AbstractTask $task, $queue);

    /**
     * @param $queue
     */
    public function work($queue);
} 