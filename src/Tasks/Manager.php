<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 13.03.14
 * Time: 6:50
 */

namespace Tasks;


class Manager {

    /**
     * @var TaskAdapter\ITaskAdapter
     */
    protected $adapter;

    /**
     * @var TaskProcessor\ITaskProcessor
     */
    protected $processor;

    /**
     * @var \Zend_Log
     */
    protected $log;

    public function __construct(\Zend_Log $log, TaskAdapter\ITaskAdapter $adapter, TaskProcessor\ITaskProcessor $processor)
    {
        $this->adapter = $adapter;
        $this->processor = $processor;
        $this->log = $log;
    }

    /**
     * @param AbstractTask $task
     * @param $queue
     * @param $runTime
     * @internal param $periodTime
     * @return bool
     */
    public function addTask(AbstractTask $task, $queue, $runTime = null)
    {
        return $this->adapter->addTask($task, $queue, $runTime);
    }

    /**
     * @param AbstractTask $task
     * @return bool
     */
    public function hasTask(AbstractTask $task)
    {
        return $this->adapter->hasTask($task);
    }

    /**
     * @param AbstractTask $task
     * @param $nextTime
     * @return bool
     */
    public function touchTask(AbstractTask $task, $nextTime)
    {
        return $this->adapter->touchTask($task, $nextTime);
    }

    /**
     * @param AbstractTask $task
     * @return bool
     */
    public function removeTask(AbstractTask $task)
    {
        return $this->adapter->removeTask($task);
    }

    /**
     * @param $consumerName
     * @param $limit
     */
    public function processTask($consumerName, $limit)
    {
        $this->log->log("Get $limit task for process", \Zend_Log::INFO, array('consumer' => $consumerName));
        if(!$tasks = $this->adapter->getTasksForProcess($consumerName, $limit)) {
            $this->log->log("Task for process not found", \Zend_Log::INFO, array('consumer' => $consumerName));
            return;
        }

        foreach($tasks as $item) {
            $class = new \ReflectionClass($item['task']);
            /** @var AbstractTask $task */
            $task  = $class->newInstanceArgs($item['params']);

            $this->processor->doTask($task, $item['queue']);

            if($period = $task->getPeriod()) {
                $nextTime = time() + $period;
                $this->adapter->touchTask($task, $nextTime);
                $this->log->log("Schedule task {$task->getId()} on ".date('c', $nextTime), \Zend_Log::INFO, array('consumer' => $consumerName));
            } else {
                $this->adapter->removeTask($task);
                $this->log->log("Complete task {$task->getId()}", \Zend_Log::INFO, array('consumer' => $consumerName));
            }
        }
    }

    /**
     * @param $queue
     */
    public function workTask($queue)
    {
        $this->processor->work($queue);
    }

    /**
     * @param AbstractTask $task
     * @param $queue
     */
    public function doTaskNow(AbstractTask $task, $queue)
    {
        $this->processor->doTask($task, $queue);
    }
}