CREATE TABLE IF NOT EXISTS `tasks` (
  `id` varchar(255) NOT NULL,
  `task` varchar(255) NOT NULL,
  `params` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `queue` varchar(255) NOT NULL,
  `consumer` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_consumer_date` (`consumer`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;